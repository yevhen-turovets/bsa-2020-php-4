<?php

namespace BinaryStudioAcademy\Game\Helpers;

final class Settings
{
    public const PLAYER_SPACESHIP_LUCK = 5;
    public const PLAYER_SPACESHIP_STRENGTH = 5;
    public const PLAYER_SPACESHIP_ARMOUR = 5;
    public const PLAYER_SPACESHIP_HEALTH = 100;
    public const HOME_GALAXY = 'home';
    public const PLAYER_SPACESHIP_NAME = 'Player Spaceship';
    public const COUNT_BUY_ARMOR = 1;
    public const COUNT_BUY_STRENGHT = 1;
    public const COUNT_RESTORE_HEALTH = 20;
    public const BOSS_NAME = 'Executor';

}
