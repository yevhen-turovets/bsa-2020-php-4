<?php

namespace BinaryStudioAcademy\Game\Helpers;

final class Hold
{
    public const SIZE = 3;
    public const CRYSTAL = '🔮';
    public const REACTOR = '🔋';
}
