<?php

namespace BinaryStudioAcademy\Game\Builder;

class Galaxy
{
    public string $galaxyName;
    public string $galaxySpaceship;

    public function getGalaxyName()
    {
        return $this->galaxyName;
    }

    public function getGalaxySpaceship()
    {
        return $this->galaxySpaceship;
    }
}
