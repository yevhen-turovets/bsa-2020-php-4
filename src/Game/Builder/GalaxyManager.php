<?php


namespace BinaryStudioAcademy\Game\Builder;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilderInterface;

class GalaxyManager
{
    private $builder;

    public function setBuilder(GalaxyBuilderInterface $builder)
    {
        $this->builder = $builder;

        return $this;
    }

    public function createHomeGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Home Galaxy')
            ->setGalaxySpaceship('player')
            ->getGalaxy();

        return $galaxy;
    }

    public function createAndromedaGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Andromeda')
            ->setGalaxySpaceship('patrol')
            ->getGalaxy();

        return $galaxy;
    }

    public function createPegasusGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Pegasus')
            ->setGalaxySpaceship('patrol')
            ->getGalaxy();

        return $galaxy;
    }

    public function createSpiralGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Spiral')
            ->setGalaxySpaceship('patrol')
            ->getGalaxy();

        return $galaxy;
    }

    public function createShiarGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Shiar')
            ->setGalaxySpaceship('battle')
            ->getGalaxy();

        return $galaxy;
    }

    public function createXenoGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Xeno')
            ->setGalaxySpaceship('battle')
            ->getGalaxy();

        return $galaxy;
    }

    public function createIsopGalaxy()
    {
        $galaxy = $this->builder
            ->setGalaxyName('Isop')
            ->setGalaxySpaceship('executor')
            ->getGalaxy();

        return $galaxy;
    }
}
