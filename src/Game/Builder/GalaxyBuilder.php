<?php

namespace BinaryStudioAcademy\Game\Builder;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilderInterface;

class GalaxyBuilder implements GalaxyBuilderInterface
{
    private Galaxy $galaxy;

    public function __construct()
    {
        $this->createGalaxy();
    }

    public function createGalaxy(): GalaxyBuilderInterface
    {
      $this->galaxy = new Galaxy();

      return $this;
    }

    public function setGalaxyName(string $val): GalaxyBuilderInterface
    {
        $this->galaxy->galaxyName = $val;

        return $this;
    }

    public function setGalaxySpaceship(string $val): GalaxyBuilderInterface
    {
        $this->galaxy->galaxySpaceship = $val;

        return $this;
    }

    public function getGalaxy(): Galaxy
    {
        $result = $this->galaxy;
        $this->createGalaxy();

        return $result;
    }
}
