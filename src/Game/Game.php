<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Commands\CommandInvoker;

class Game
{
    private Random $random;
    private CommandInvoker $invoker;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->invoker = new CommandInvoker();
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Welcome to the "Galaxy Warriors"');
        $writer->writeln('You control a military reconnaissance spaceship that needs to find and destroy the main enemy alien spaceship in one of the galaxies.');
        $writer->writeln("Enter 'help' - to see all available commands!");

        $input = strtolower(trim($reader->read()));

        while (true) {
            $this->checkAndRun($input, $writer);

            $input = trim($reader->read());
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $input = strtolower(trim($reader->read()));

        $this->checkAndRun($input, $writer);
    }

    private function checkAndRun(string $input, Writer $writer)
    {
        $command = preg_split('/\s+/', $input)[0];
        $params = preg_split('/\s+/', $input)[1] ?? null;

        $this->invoker->runCommand($command, $params, $this->random, $writer);
    }
}
