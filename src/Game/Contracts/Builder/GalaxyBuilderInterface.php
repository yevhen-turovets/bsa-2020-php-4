<?php

namespace BinaryStudioAcademy\Game\Contracts\Builder;

use BinaryStudioAcademy\Game\Builder\Galaxy;

interface GalaxyBuilderInterface
{
    public function createGalaxy(): GalaxyBuilderInterface;

    public function setGalaxyName(string $val): GalaxyBuilderInterface;

    public function setGalaxySpaceship(string $val): GalaxyBuilderInterface;

    public function getGalaxy(): Galaxy;
}
