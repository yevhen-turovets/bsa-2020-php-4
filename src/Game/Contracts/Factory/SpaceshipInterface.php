<?php

namespace BinaryStudioAcademy\Game\Contracts\Factory;

interface SpaceshipInterface
{
    public function getName(): string;

    public function getStrength(): int;

    public function getArmor(): int;

    public function getLuck(): int;

    public function getHealth(): int;

    public function getHold(): array;
}
