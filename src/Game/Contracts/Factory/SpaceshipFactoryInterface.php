<?php

namespace BinaryStudioAcademy\Game\Contracts\Factory;

use BinaryStudioAcademy\Game\Factory\Spaceships\Spaceship;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

interface SpaceshipFactoryInterface
{
    public function createSpaceship(string $type, Random $random): Spaceship;
}
