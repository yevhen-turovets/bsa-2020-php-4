<?php

namespace BinaryStudioAcademy\Game\Contracts\Commands;

interface CommandInterface
{
    public function execute(?string $params): void;
}
