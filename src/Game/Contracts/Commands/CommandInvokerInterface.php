<?php

namespace BinaryStudioAcademy\Game\Contracts\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

interface CommandInvokerInterface
{
    public function runCommand(
        string $command,
        ?string $params,
        Random $random,
        Writer $writer
    );
}
