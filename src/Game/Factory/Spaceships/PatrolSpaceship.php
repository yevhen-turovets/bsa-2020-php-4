<?php

namespace BinaryStudioAcademy\Game\Factory\Spaceships;

use BinaryStudioAcademy\Game\Helpers\Hold;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Stats;

class PatrolSpaceship extends Spaceship
{
    protected string $name = 'Patrol Spaceship';

    public function __construct(Random $random)
    {
        $math = new Math();

        $this->strength = $math->spaceshipStat($random, 3, 4);
        $this->armor = $math->spaceshipStat($random, 2, 4);
        $this->luck = $math->spaceshipStat($random, 1, 2);
        $this->health = Stats::MAX_HEALTH;
        $this->hold = [Hold::REACTOR, '', ''];
    }
}
