<?php

namespace BinaryStudioAcademy\Game\Factory\Spaceships;

use BinaryStudioAcademy\Game\Helpers\Settings;
use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademy\Game\Helpers\Hold;


class PlayerSpaceship extends Spaceship
{
    protected int $strength = Settings::PLAYER_SPACESHIP_STRENGTH;
    protected int $armor = Settings::PLAYER_SPACESHIP_ARMOUR;
    protected int $luck = Settings::PLAYER_SPACESHIP_LUCK;
    protected int $health = Settings::PLAYER_SPACESHIP_HEALTH;
    protected array $hold = ['', '', ''];
    protected string $name = Settings::PLAYER_SPACESHIP_NAME;
    protected string $currentGalaxy = Settings::HOME_GALAXY;
    protected static $instance;

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct() {}

    private function __clone() {}

    private function __wakeup() {}

    public function restart()
    {
        $this->strength = Settings::PLAYER_SPACESHIP_STRENGTH;
        $this->armor = Settings::PLAYER_SPACESHIP_ARMOUR;
        $this->luck = Settings::PLAYER_SPACESHIP_LUCK;
        $this->health = Settings::PLAYER_SPACESHIP_HEALTH;
        $this->hold = ['', '', ''];
        $this->currentGalaxy = Settings::HOME_GALAXY;
    }

    public function getCurrentGalaxy(): string
    {
        return $this->currentGalaxy;
    }

    public function setGalaxy(string $galaxy)
    {
        $this->currentGalaxy = $galaxy;
    }

    public function updateHold(array $newHold)
    {
        $myItems = array_filter($this->hold);
        $newItems = array_filter($newHold);
        $this->hold = array_merge($myItems, $newItems);

        while (count($this->hold) !== Hold::SIZE) {
            $this->hold[] = '';
        }
    }

    public function buyStrength(int $index)
    {
        $this->strength += Settings::COUNT_BUY_STRENGHT;
        $this->deleteCrystal($index);
    }

    public function buyArmor(int $index)
    {
        $this->armor += Settings::COUNT_BUY_ARMOR;
        $this->deleteCrystal($index);
    }

    protected function deleteCrystal(int $index)
    {
        $this->hold[$index] = '';
    }

    public function buyReactor(int $index)
    {
        $this->hold[$index] = Hold::REACTOR;
    }

    public function getReactorsAmount()
    {
        $counter = 0;

        foreach ($this->hold as $item) {
            if ($item === Hold::REACTOR) {
                $counter++;
            }
        }

        return $counter;
    }

    public function restoreHealth(int $index)
    {
        $this->health += Settings::COUNT_RESTORE_HEALTH;

        if ($this->health > Stats::MAX_HEALTH) {
            $this->health = Stats::MAX_HEALTH;
        }

        $this->deleteReactor($index);
    }

    protected function deleteReactor(int $index)
    {
        $this->hold[$index] = '';
    }
}
