<?php

namespace BinaryStudioAcademy\Game\Factory\Spaceships;

use BinaryStudioAcademy\Game\Contracts\Factory\SpaceshipInterface;
use BinaryStudioAcademy\Game\Helpers\Settings;

abstract class Spaceship implements SpaceshipInterface
{
    protected string $name;
    protected int $strength;
    protected int $armor;
    protected int $luck;
    protected int $health;
    protected array $hold;

    public function getName(): string
    {
        return $this->name;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function stats(): string
    {
        return "Spaceship stats:" . PHP_EOL
            . "strength: {$this->strength}" . PHP_EOL
            . "armor: {$this->armor}" . PHP_EOL
            . "luck: {$this->luck}" . PHP_EOL
            . "health: {$this->health}" . PHP_EOL
            . "hold: {$this->holdPresenter()}" . PHP_EOL;
    }

    public function holdPresenter(): string
    {
        if (empty($this->hold)) {
            $hold = '[ _ _ _ ]';
        } else {
            $hold = '[ ';

            foreach ($this->hold as $item) {
                if ($item) {
                    $hold .= $item . ' ';
                } else {
                    $hold .= '_ ';
                }
            }

            $hold .= ']';
        }

        return $hold;
    }

    public function makeDamage(int $damage)
    {
        $this->health -= $damage;
    }

    public function isBoss()
    {
        return $this->name === Settings::BOSS_NAME;
    }

    public function grabHoldPresenter(): string
    {
        $hold = '';

        foreach ($this->hold as $item) {
            if ($item) {
                $hold .= ' ' . $item;
            }
        }

        return $hold;
    }

    public function getHoldWeight(): int
    {
        $counter = 0;

        foreach ($this->hold as $item) {
            if ($item) {
                $counter++;
            }
        }

        return $counter;
    }
}
