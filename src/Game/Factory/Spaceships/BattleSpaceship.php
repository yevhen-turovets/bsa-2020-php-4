<?php

namespace BinaryStudioAcademy\Game\Factory\Spaceships;

use BinaryStudioAcademy\Game\Helpers\Hold;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Stats;

class BattleSpaceship extends Spaceship
{
    protected string $name = 'Battle Spaceship';

    public function __construct(Random $random)
    {
        $math = new Math();

        $this->strength = $math->spaceshipStat($random, 5, 8);
        $this->armor = $math->spaceshipStat($random, 6, 8);
        $this->luck = $math->spaceshipStat($random, 3, 6);
        $this->health = Stats::MAX_HEALTH;
        $this->hold = [Hold::REACTOR, Hold::CRYSTAL, ''];
    }
}
