<?php

namespace BinaryStudioAcademy\Game\Factory;

use BinaryStudioAcademy\Game\Factory\Spaceships\BattleSpaceship;
use BinaryStudioAcademy\Game\Factory\Spaceships\ExecutorSpaceship;
use BinaryStudioAcademy\Game\Factory\Spaceships\PatrolSpaceship;
use BinaryStudioAcademy\Game\Contracts\Factory\SpaceshipFactoryInterface;
use BinaryStudioAcademy\Game\Factory\Spaceships\Spaceship;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class SpaceshipFactory implements SpaceshipFactoryInterface
{
    public function createSpaceship(string $type, Random $random): Spaceship
    {
        switch ($type) {
            case 'patrol':
                return new PatrolSpaceship($random);
                break;
            case 'battle':
                return new BattleSpaceship($random);
                break;
            case 'executor':
                return new ExecutorSpaceship();
                break;
        }
    }
}
