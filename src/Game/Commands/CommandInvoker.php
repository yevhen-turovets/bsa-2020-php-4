<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\CommandInvokerInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Messages;

class CommandInvoker implements CommandInvokerInterface
{
    private static array $commands = [
        'help',
        'exit',
        'restart',
        'stats',
        'whereami',
        'set-galaxy',
        'attack',
        'grab',
        'buy',
        'apply-reactor',
    ];

    public function runCommand(string $command, ?string $params, Random $random, Writer $writer)
    {
        if (in_array($command, self::$commands)) {
            $commandClass = $this->getCommandClassName($command);

            if (class_exists($commandClass)) {
                $commandObject = new $commandClass($random, $writer);
                $commandObject->execute($params);
            }
        } else {
            $writer->writeln(Messages::errors('unknown_command', $command));
        }
    }

    private function getCommandClassName(string $command)
    {
        return __NAMESPACE__ . '\\' . 'Command\\' .
            implode('', array_map('ucfirst', explode('-', $command)))
            . 'Command';
    }
}
