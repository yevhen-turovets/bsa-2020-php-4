<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Builder\GalaxyBuilder;
use BinaryStudioAcademy\Game\Builder\GalaxyManager;
use BinaryStudioAcademy\Game\Contracts\Commands\CommandInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Factory\Spaceships\Spaceship;
use BinaryStudioAcademy\Game\Factory\Spaceships\PlayerSpaceship;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Settings;

abstract class Command implements CommandInterface
{
    protected Random $random;
    protected Writer $writer;
    protected PlayerSpaceship $player;
    protected static ?Spaceship $enemy = null;
    protected static ?Spaceship $diedEnemy = null;
    protected static bool $grabbed = false;

    public function __construct(Random $random, Writer $writer)
    {
        $this->random = $random;
        $this->writer = $writer;
        $this->player = PlayerSpaceship::getInstance();
    }

    protected function isHome()
    {
        return $this->player->getCurrentGalaxy() === Settings::HOME_GALAXY;
    }

    protected function getGalaxy($galaxyCode)
    {
        $builder = new GalaxyBuilder();
        $manager = new GalaxyManager();
        $manager->setBuilder($builder);

        $getGalaxyMethod = 'create' . ucfirst($galaxyCode) . 'Galaxy';
        $galaxy = $manager->$getGalaxyMethod();

        return $galaxy;
    }
}
