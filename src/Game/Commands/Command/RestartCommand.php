<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Messages;

final class RestartCommand extends Command
{
    public function execute(string $params = null): void
    {
        $this->player->restart();
        $this->writer->writeln(Messages::restart());
    }
}
