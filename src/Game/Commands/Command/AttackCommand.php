<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Messages;

final class AttackCommand extends Command
{
    public function execute(string $params = null): void
    {
        $math = new Math();

        if (!$this->isHome()) {
            $enemy = self::$enemy;

            $playerLuck = $math->luck($this->random, $this->player->getLuck());
            $playerDamage = $playerLuck ? $math->damage($this->player->getStrength(), $enemy->getArmor()) : 0;

            $enemyLuck = $math->luck($this->random, $enemy->getLuck());
            $enemyDamage = $enemyLuck ? $math->damage($enemy->getStrength(), $this->player->getArmor()) : 0;

            $enemy->makeDamage($playerDamage);

            if ($enemy->getHealth() <= 0 && !self::$diedEnemy && !self::$grabbed) {
                self::$diedEnemy = $enemy;

                if ($enemy->isBoss()) {
                    $this->writer->writeln(Messages::finalWin());
                } else {
                    $this->writer->writeln(Messages::destroyed($enemy->getName()));
                }
            } elseif ($enemy->getHealth() > 0) {
                $this->player->makeDamage($enemyDamage);

                if ($this->player->getHealth() <= 0) {
                    $this->writer->writeln(Messages::die());
                    $this->player->restart();
                } else {
                    $this->writer->writeln(Messages::attack(
                        $enemy->getName(),
                        $playerDamage,
                        $enemy->getHealth(),
                        $enemyDamage,
                        $this->player->getHealth()
                    ));
                }
            } elseif (self::$diedEnemy || !self::$diedEnemy && self::$grabbed) {
                $this->writer->writeln(Messages::errors('enemy_killed'));
            } elseif (self::$grabbed) {
                $this->writer->writeln(Messages::errors('enemy_grabbed'));
            }
        }
        else {
            $this->writer->writeln(Messages::errors('home_galaxy_attack'));
        }
    }
}
