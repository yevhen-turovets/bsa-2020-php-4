<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Messages;

final class WhereamiCommand extends Command
{
    public function execute(string $params = null): void
    {
        $galaxy = $this->getGalaxy($this->player->getCurrentGalaxy());

        $this->writer->writeln(Messages::whereAmI($galaxy));
    }
}
