<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Hold;
use BinaryStudioAcademy\Game\Helpers\Messages;
use BinaryStudioAcademy\Game\Helpers\Stats;

final class BuyCommand extends Command
{
    private array $params = [
        'strength',
        'armor',
        'reactor'
    ];

    public function execute(?string $param): void
    {
        $crystalIndex = $this->getCrystalIndexIfExists();

        if ($this->isHome()) {
            if (!$param) {
                $this->writer->writeln(Messages::errors('not_enter_buy'));
            } elseif (!in_array($param, $this->params)) {
                $this->writer->writeln(Messages::errors('havnt_item'));
            } elseif ($crystalIndex === null) {
                echo $crystalIndex;
                $this->writer->writeln(Messages::errors('havnt_crystals'));
            } else {
                $this->buyAction($param, $crystalIndex);
            }
        } else {
            $this->writer->writeln(Messages::errors('buy_in_home'));
        }
    }

    private function getCrystalIndexIfExists(): ?int
    {
        $index = null;

        for ($i = 0; $i < Hold::SIZE; $i++) {
            if ($this->player->getHold()[$i] === Hold::CRYSTAL) {
                $index = $i;
                break;
            }
        }

        return $index;
    }

    private function buyAction(string $param, int $crystalIndex)
    {
        switch ($param) {
            case 'strength':
                if ($this->player->getStrength() === Stats::MAX_STRENGTH) {
                    $this->writer->writeln(Messages::errors('max_strength'));
                } else {
                    $this->player->buyStrength($crystalIndex);
                    $this->writer->writeln(Messages::buySkill('strenght', $this->player->getStrength()));
                }
                break;
            case 'armor':
                if ($this->player->getArmor() === Stats::MAX_ARMOUR) {
                    $this->writer->writeln(Messages::errors('max_armor'));
                } else {
                    $this->player->buyArmor($crystalIndex);
                    $this->writer->writeln(Messages::buySkill('armor', $this->player->getArmor()));
                }
                break;
            case 'reactor':
                $this->player->buyReactor($crystalIndex);
                $this->writer->writeln(Messages::buyReactor($this->player->getReactorsAmount()));
                break;
        }
    }
}
