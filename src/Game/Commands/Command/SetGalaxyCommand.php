<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Factory\SpaceshipFactory;
use BinaryStudioAcademy\Game\Helpers\Messages;

final class SetGalaxyCommand extends Command
{
    private array $params = [
        'home',
        'andromeda',
        'spiral',
        'pegasus',
        'shiar',
        'xeno',
        'isop',
    ];

    public function execute(?string $params): void
    {
        if ($params) {
            if (in_array($params, $this->params)) {
                if ($this->player->getCurrentGalaxy() == $params) {
                    $this->writer->writeln(Messages::errors('now_hear'));
                } else {
                    $this->player->setGalaxy($params);
                    $galaxy = $this->getGalaxy($this->player->getCurrentGalaxy());

                    self::$diedEnemy = null;
                    self::$grabbed = false;

                    if ($this->isHome()) {
                        self::$enemy = null;

                        $this->writer->writeln(Messages::homeGalaxy());
                    } else {
                        self::$enemy = $this->getGalaxySpaceship($galaxy->getGalaxySpaceship());

                        $this->writer->writeln(Messages::galaxy($galaxy, self::$enemy));
                    }
                }
            } else {
                $this->writer->writeln(Messages::errors('undefined_galaxy'));
            }
        } else {
            $this->writer->writeln(Messages::errors('not_enter_galaxy'));
        }
    }

    private function getGalaxySpaceship($spaceshipType)
    {
        $spaceshipFactory = new SpaceshipFactory();
        $spaceship = $spaceshipFactory->createSpaceship($spaceshipType, $this->random);

        return $spaceship;
    }
}
