<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Messages;

final class ExitCommand extends Command
{
    public function execute(string $params = null): void
    {
        $this->writer->writeln(Messages::exit());
        exit();
    }
}
