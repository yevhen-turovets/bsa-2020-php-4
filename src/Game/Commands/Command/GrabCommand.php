<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Messages;
use BinaryStudioAcademy\Game\Helpers\Hold;

final class GrabCommand extends Command
{
    public function execute(string $params = null): void
    {
        if (self::$grabbed) {
            $this->writer->writeln(Messages::errors('already_grabbed'));
        } elseif ($this->isHome()) {
            $this->writer->writeln(Messages::errors('home_galaxy_grab'));
        } elseif (!self::$diedEnemy) {
            $this->writer->writeln(Messages::errors('grab_undestroyed_spaceship'));
        } elseif (self::$diedEnemy->getHoldWeight() == 0) {
            $this->writer->writeln(Messages::errors('grab_empty_spaceship'));
        } elseif ($this->player->getHoldWeight() >= Hold::SIZE) {
            $this->writer->writeln(Messages::errors('full_hold'));
        } else {
                $this->player->updateHold(self::$diedEnemy->getHold());
                self::$grabbed = true;

                $this->writer->writeln(Messages::grab(self::$diedEnemy));
        }
    }
}
