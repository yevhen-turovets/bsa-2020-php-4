<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Hold;
use BinaryStudioAcademy\Game\Helpers\Messages;
use BinaryStudioAcademy\Game\Helpers\Stats;

final class ApplyReactorCommand extends Command
{
    public function execute(string $params = null): void
    {
        $reactorIndex = $this->getReactorIndexIfExists();

        if ($reactorIndex === null) {
            $this->writer->writeln(Messages::errors('havent_reactors'));
        } elseif ($this->player->getHealth() === Stats::MAX_HEALTH) {
            $this->writer->writeln(Messages::errors('max_health'));
        } else {
            $this->player->restoreHealth($reactorIndex);
            $this->writer->writeln(Messages::applyReactor($this->player->getHealth()));
        }
    }

    private function getReactorIndexIfExists(): ?int
    {
        $index = null;

        for ($i = 0; $i < Hold::SIZE; $i++) {
            if ($this->player->getHold()[$i] === Hold::REACTOR) {
                $index = $i;
                break;
            }
        }

        return $index;
    }
}
