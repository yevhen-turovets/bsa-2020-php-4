<?php

namespace BinaryStudioAcademy\Game\Commands\Command;

use BinaryStudioAcademy\Game\Commands\Command;

final class StatsCommand extends Command
{
    public function execute(string $params = null): void
    {
        $this->writer->writeln($this->player->stats());
    }
}
